package ca.candrade.exceptions;

/**
 * In division the divisor equals zero
 *
 * @author Ken Fogel
 */
public class DivisionByZero extends Exception {
    
    public DivisionByZero(String msg) {
        super(msg);
    }

}
