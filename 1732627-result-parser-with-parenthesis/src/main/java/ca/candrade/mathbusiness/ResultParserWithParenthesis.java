package ca.candrade.mathbusiness;

import ca.candrade.exceptions.DivisionByZero;
import ca.candrade.exceptions.InvalidInputQueueString;
import ca.candrade.exceptions.NonBinaryExpression;
import ca.candrade.exceptions.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Queue;

/**
 * A result parser class which allows for the use of braces with operations.
 * Using this class one could convert an infix queue to postfix and evaluate a 
 * postfix queue.
 * 
 * @author Christian Andrade 1732627
 */
public class ResultParserWithParenthesis {

    /**
     * Method which converts the provided queue of strings into postfix. 
     * Ex: (2+3) => 23+
     * @param infixQueue a queue of strings.
     * @return The postfix queue of strings.
     * @throws NonMatchingParenthesis
     * @throws InvalidInputQueueString
     * @throws NonBinaryExpression
     */
    public Queue<String> convertInfixToPostfix(Queue<String> infixQueue) throws NonMatchingParenthesis, InvalidInputQueueString, NonBinaryExpression {
        infixQueue = correctBraceLogic(infixQueue);
        validations(infixQueue);
        Queue<String> postFix = generatePostFix(infixQueue);
        return postFix;
    }

    /**
     * Method which evaluates the numeric value of a postfix queue of strings and
     * returns a string representation of said value.
     * @param postfixQueue The postfix queue of strings.
     * @return a string representation of a postfix queue of strings.
     * @throws DivisionByZero
     */
    public String evaluatePostfix(Queue<String> postfixQueue) throws DivisionByZero {
        Deque<String> operands = new ArrayDeque<String>();

        while (!postfixQueue.isEmpty()) {
            if (operatorCheck(postfixQueue.peek())) {
                if (operands.size() != 1) {
                    operands.push(performCalculation(operands.pop(), operands.pop(), postfixQueue.remove()) + "");
                } else {
                    String posNeg = postfixQueue.remove();
                    if (posNeg.equals("+")) {
                        return "" + operands.pop();
                    } else {
                        return posNeg + "" + operands.pop();
                    }
                }
            } else {
                operands.push(postfixQueue.remove());
            }
        }
        return operands.pop();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELPERS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    private Queue<String> generatePostFix(Queue<String> infixQueue) {
        Queue<String> postFix = new ArrayDeque<>();
        Deque<String> operators = new ArrayDeque<String>();
        while (!infixQueue.isEmpty()) {
            String removed = infixQueue.remove();
            if (operatorCheck(removed)) {
                operandLogic(removed, operators, postFix);
            } else {
                postFix.add(removed);
            }
        }
        while (!operators.isEmpty()) {
            postFix.add(operators.pop());
        }
        return postFix;
    }

    private void operandLogic(String removed, Deque<String> operators, Queue<String> postFix) {
        if (operators.isEmpty()) operators.push(removed);
        else {
            if (removed.equals(")"))
                while (!operators.isEmpty()) {
                    String operator = operators.pop();
                    if (!operator.equals("(")) {
                        postFix.add(operator);
                        if (operators.peek().equals("(")) {
                            operators.pop();
                            break;
                        }
                    } else break;
                }
            else if (removed.equals("(")) operators.push(removed);
            else if (operators.peek().equals("(")) operators.push(removed);
            else if (operatorComparison(removed, operators.peek())) operators.push(removed);
            else {
                postFix.add((String) operators.pop());
                operators.push(removed);
            }
        }
    }

    private boolean operatorComparison(String operatorOne, String operatorTwo) {
        return getOperatorValue(operatorOne) > getOperatorValue(operatorTwo);
    }

    private int getOperatorValue(String operator) {
        if (operator.equals("(")) {
            return 0;
        } else if (operator.equals("*") || operator.equals("/")) {
            return 2;
        }
        return 1;
    }

    private boolean operatorCheck(String operator) {
        return operator.equals("/") || operator.equals("*") || operator.equals("+") || operator.equals("-") || operator.equals("(") || operator.equals(")");
    }

    private Queue<String> correctMultiplicationForBrackets(Queue<String> infixQueue) {
        Queue<String> newQueue = new ArrayDeque<>();
        String previous = "";
        while (!infixQueue.isEmpty()) {
            String removed = infixQueue.remove();
            if (!newQueue.isEmpty())
                if (((!previous.equals("*") && !previous.equals("-") && !previous.equals("+") && !previous.equals("/") && !previous.equals("(") && !previous.equals(")")) && removed.equals("("))) newQueue.add("*");
            newQueue.add(removed);
            previous = removed;
        }
        return newQueue;
    }
    
    private Queue<String> correctBracePositioning(Queue<String> infixQueue) {
        Queue<String> newQueue = new ArrayDeque<>();
        String previous = "";
        while (!infixQueue.isEmpty()) {
            String removed = infixQueue.remove();
            if (removed.equals("-(")) {
                newQueue.add("-1");
                newQueue.add("*");
                newQueue.add("(");
            } else if (removed.equals("+(")) newQueue.add("(");
            else if (removed.equals(")"))
                if (previous.equals(")")) newQueue.add(")");
                else previous = ")";
            else if (previous.equals(")") && removed.equals("(")) {
                newQueue.add(")");
                newQueue.add("*");
                newQueue.add("(");
                previous = "";
            } else if (previous.equals(")") && !removed.equals("(")) {
                newQueue.add(")");
                newQueue.add(removed);
                previous = "";
            } else newQueue.add(removed);
        }
        if (previous.equals(")")) newQueue.add(")");
        return newQueue;
    }

    private Queue<String> correctBraceLogic(Queue<String> infixQueue) {
        return correctBracePositioning(correctMultiplicationForBrackets(infixQueue));
    }

    private double performCalculation(String operandTwo, String operandOne, String operator) throws DivisionByZero {
        double result = 0;
        switch (operator) {
            case "+" -> {
                result = Double.parseDouble(operandOne) + Double.parseDouble(operandTwo);
                break;
            }
            case "-" -> {
                result = Double.parseDouble(operandOne) - Double.parseDouble(operandTwo);
                break;
            }
            case "*" -> {
                result = Double.parseDouble(operandOne) * Double.parseDouble(operandTwo);
                break;
            }
            case "/" -> {
                if (Double.parseDouble(operandTwo) != 0.0) {
                    result = Double.parseDouble(operandOne) / Double.parseDouble(operandTwo);
                    break;
                } else {
                    throw new DivisionByZero("One of the operands in the expression is 0.");
                }
            }
        }
        return result;
    }

    private List<Boolean> translateQueueToOperatorListBoolean(Queue<String> infixQueue) {
        List<Boolean> operators = new ArrayList<Boolean>();
        for (String s : infixQueue) {
            if (operatorCheck(s)) {
                operators.add(true);
            } else {
                operators.add(false);
            }
        }
        return operators;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~VALIDATION~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    private void validations(Queue<String> infixQueue) throws InvalidInputQueueString, NonMatchingParenthesis, NonBinaryExpression {
        validateInputQueueString(infixQueue);
        validateBinaryExpression(infixQueue);
        validateMatchingParenthesis(infixQueue);
    }

    private void validateInputQueueString(Queue<String> infixQueue) throws InvalidInputQueueString {
        if (infixQueue == null || infixQueue.isEmpty()) {
            throw new InvalidInputQueueString("Expression can not be empty or null.");
        }
        for (String operation : infixQueue) {
            if (operation.length() > 1) {
                validateCompoundString(operation);
            }
            validateChars(operation);
            validateDecimals(operation);
        }
    }

    private void validateCompoundString(String compound) throws InvalidInputQueueString {
        if (!areAllCharsNumbers(compound)) {
            isMixedCompound(compound);
        }
    }

    private void isMixedCompound(String compound) throws InvalidInputQueueString {
        char[] operationChars = compound.toCharArray();
        if (operationChars[0] != '-' && operationChars[0] != '(' && operationChars[0] != '+') {
            throw new InvalidInputQueueString("Expression can not start without either a number, negative sign '-', positive sign '+', or an opening brace '('.");
        }
    }

    private boolean areAllCharsNumbers(String compound) {
        char[] operationChars = compound.toCharArray();
        boolean areAllCharsNumbers = false;
        for (char c : operationChars) {
            if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9') {
                areAllCharsNumbers = true;
            } else {
                break;
            }
        }
        return areAllCharsNumbers;
    }

    private void validateChars(String operation) throws InvalidInputQueueString {
        for (char c : operation.toCharArray()) {
            /* java regex is garbage so I'm checking each char manually.
            in any other language ((?!([0-9])|((\()|(\*)|(\+)|(\-)|(\/)|(\)))).)+
            would work to test this*/
            if (!(c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9' || c == '(' || c == ')' || c == '*' || c == '/' || c == '-' || c == '+' || c == '.')) {
                throw new InvalidInputQueueString("Invalid character \"" + c + "\" found in the provided expression.");
            }
        }
    }

    private void validateDecimals(String operation) throws InvalidInputQueueString {
        char previousCharacter = ' ';
        for (char c : operation.toCharArray()) {
            if (previousCharacter == '.') {
                if (!(c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9')) {
                    throw new InvalidInputQueueString("Incomplete decimal found in the provided expression.");
                }
            }
        }
    }

    private void validateBinaryExpression(Queue<String> infixQueue) throws NonBinaryExpression {
        validateDuplicateOperators(infixQueue);
        validateBeginningOfExpression(infixQueue);
        validateEndOfExpression(infixQueue);
    }
    
    private void validateDuplicateOperators(Queue<String> infixQueue) throws NonBinaryExpression {
        List<Boolean> operators = translateQueueToOperatorListBoolean(infixQueue);
        for (int i = 1; i < operators.size(); i -= -1) {
            if (operators.get(i - 1).equals(operators.get(i))) {
                if (!(infixQueue.toArray()[i - 1].toString().equals("(")
                        || infixQueue.toArray()[i - 1].toString().equals(")")
                        || infixQueue.toArray()[i].toString().equals("(")
                        || infixQueue.toArray()[i].toString().equals(")"))
                        || (infixQueue.toArray()[i - 1].toString().equals("(") && infixQueue.toArray()[i].toString().equals(")"))
                        || (infixQueue.toArray()[i - 1].toString().equals(")") && infixQueue.toArray()[i].toString().equals("("))) {
                    throw new NonBinaryExpression("Duplicate operators in expression.");
                }
            }
        }
    }
    
    private void validateBeginningOfExpression(Queue<String> infixQueue) throws NonBinaryExpression {
        String firstString = infixQueue.toArray()[0].toString();
        if (firstString.equals("/") || firstString.equals("*")) {
            throw new NonBinaryExpression("Found operator at start of expression.");
        }
    }
    
    private void validateEndOfExpression(Queue<String> infixQueue) throws NonBinaryExpression {
        String lastString = infixQueue.toArray()[infixQueue.toArray().length-1].toString();
        if (lastString.equals("/") || lastString.equals("*") || lastString.equals("+") || lastString.equals("-")) {
            throw new NonBinaryExpression("Found operator at end of expression.");
        }
    }

    private void validateMatchingParenthesis(Queue<String> infixQueue) throws NonMatchingParenthesis {
        int openBraces = 0;
        int closingBraces = 0;
        for (String s : infixQueue) {
            for (char c : s.toCharArray()) {
                if (closingBraces <= openBraces) {
                    if (c == '(') {
                        openBraces++;
                    } else if (c == ')') {
                        closingBraces++;
                    }
                } else {
                    throw new NonMatchingParenthesis("Found closing brace with no opening brace.");
                }
            }
        }
        if (openBraces != closingBraces) {
            throw new NonMatchingParenthesis("Uneven braces. Found " + openBraces + " open braces with " + closingBraces + " closing braces.");
        }
    }
}